\" txt2man -d "25 Sep 2016" -t bruteforce-salted-openssl -r bruteforced-salted-openssl-1.3.1
\" -s 1 -v "try to find the passphrase for files encrypted with OpenSSL" bruteforce-salted-openssl.txt
\" > bruteforce-salted-openssl.1

NAME
  bruteforce-salted-openssl - try to find the passphrase for files encrypted with OpenSSL

SYNOPSIS
  bruteforce-salted-openssl [options] <filename>

DESCRIPTION
  bruteforce-salted-openssl try to find the passphrase or password of a file that was
  encrypted with the openssl command. It can be used in two ways:

   * Try all possible passwords given a charset.
   * Try all passwords in a file (dictionary).

  bruteforce-salted-openssl have the following features:

   * You can specify the number of threads to use when cracking a file.
   * The program should be able to use all the digests and symmetric ciphers available
     with the OpenSSL libraries installed on your system.
   * Sending a USR1 signal to a running bruteforce-salted-openssl process makes it print
     progress and continue.
   * There are an exhaustive mode and a dictionary mode.

  In the exhaustive mode the program tries to decrypt the file by trying all possible
  passwords. It is especially useful if you know something about the password (i.e. you
  forgot a part of your password but still remember most of it). Finding the password of
  the file without knowing anything about it would take way too much time (unless the
  password is really short and/or weak). There are some command line options to specify:

   * The minimum password length to try.
   * The maximum password length to try.
   * The beginning of the password.
   * The end of the password.
   * The character set to use (among the characters of the current locale).

  In dictionary mode the program tries to decrypt the file by trying all the passwords
  contained in a file. The file must have one password per line.

OPTIONS
  -1           Stop the program after finding the first password candidate.
  -a           List the available cipher and digest algorithms.
  -B <file>    Search using binary passwords (instead of character passwords).
               Write candidates to <file>.
  -b <string>  Beginning of the password. The default value is "".
  -c <cipher>  Cipher for decryption. The default value is aes-256-cbc.
  -d <digest>  Digest for key and initialization vector generation. Default: md5.
  -e <string>  End of the password. Default: "".
  -f <file>    Read the passwords from a file instead of generating them.
  -h           Show help and quit.
  -L <n>       Limit the maximum number of tested passwords to <n>.
  -l <length>  Minimum password length (beginning and end included). Default: 1.
  -M <string>  Consider the decryption as successful when the data starts with <string>.
               Without this option, the decryption is considered as successful when the
               data contains mostly printable ASCII characters (at least 90%).
  -m <length>  Maximum password length (beginning and end included). Default: 8.
  -N           Ignore decryption errors (similar to openssl -nopad).
  -p <n>       Print a message every <n> passwords tested.
  -s <string>  Password character set. Default value is "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
  -t <n>       Number of threads to use. Default: 1.

  Note: Sending a USR1 signal to a running bruteforce-salted-openssl process makes it print
  progress info to standard error and continue.

LIMITATIONS
  The program considers decrypted data as correct if it is mainly composed of printable
  ASCII characters (at least 90%). If the file you want to decrypt doesn't contain plain
  text, you will have to either use the -M option, or modify the 'valid_data' function
  in the source code to match your needs.

  If the file you want to decrypt is big, you should use the -N option on a truncated
  version of the file (to avoid decrypting the whole file with each password).

EXAMPLES
  Try to find the password of an aes256 encrypted file using 4 threads, trying only
  passwords with 5 characters:

    $ bruteforce-salted-openssl -t 4 -l 5 -m 5 -c aes256 encrypted.file

  Try to find the password of an aes256 encrypted file using 4 threads, trying only
  passwords with 5 characters and print the password currently tried every 100000 attempts:

    $ bruteforce-salted-openssl -t 4 -l 5 -m 5 -c aes256 -p 100000 encrypted.file

  Try to find the password of a des3 encrypted file using 8 threads, trying only passwords
  with 9 to 11 characters, beginning with "AbCD", ending with "Ef", and containing only letters:

    $ bruteforce-salted-openssl -t 8 -l 9 -m 11 -c des3 -b "AbCD" -e "Ef" -s "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" encrypted.file

  Try to find the password of an aes256 encrypted file using 6 threads, trying the passwords contained in a dictionary file:

    $ bruteforce-salted-openssl -t 6 -f dictionary.txt -c aes256 encrypted-file

  Show the list of available algorithms:

    $ bruteforce-salted-openssl -a

  If the program finds a candidate password 'pwd', you can decrypt the data using the 'openssl' command:

    $ openssl enc -d -aes256 -salt -in encrypted.file -out decrypted.file -k pwd

DONATIONS
  If you find this program useful and want to make a donation, you can send coins to one of the following addresses:

  * Peercoin: PWFNV1Cvq7nQBRyRueuYzwmDNXUGpgNkBC
  * Bitcoin: 1F1ZfM7XtggHsShK4vwuy9zv98a9wt7nXx

AUTHOR
  bruteforce-salted-openssl was written by Guillaume LE VAILLANT. For contact, use the email <guillaume.le.vaillant@openmailbox.org>
  or go to https://github.com/glv2/bruteforce-salted-openssl.

  This manual page was written by Joao Eriberto Mota Filho <eriberto@debian.org> for the Debian project (but may be used by others).
