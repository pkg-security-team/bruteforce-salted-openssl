bruteforce-salted-openssl (1.4.2-4) unstable; urgency=medium

  * Team upload.

  [ Joao Eriberto Mota Filho ]
  * debian/control: bumped Standards-Version to 4.6.1.
  * debian/copyright:
      - Converted the last paragraph of the GPL-3 in a comment.
      - Updated packaging copyright years.
  * debian/watch: updated the search rule to make it compliant with new
    standards of the GitHub.

  [ Debian Janitor ]
  * debian/control: Remove empty control field Uploaders.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Bump debhelper from old 12 to 13.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 21 Dec 2022 23:56:23 -0300

bruteforce-salted-openssl (1.4.2-3) unstable; urgency=medium

  * Team upload.

  [ Joao Eriberto Mota Filho ]
  * debian/control:
      - Bumped Standards-Version to 4.5.0.
      - Removed my name from Uploaders field.
  * debian/copyright: updated packaging copyright years.

  [ Samuel Henrique ]
  * Configure git-buildpackage for Debian.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 24 Feb 2020 19:48:41 -0300

bruteforce-salted-openssl (1.4.2-2) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.4.1.
  * debian/copyright: added packaging rights for Samuel Henrique.
  * debian/watch: using variables instead of expressions.

  [ Samuel Henrique ]
  * Add salsa-ci.yml.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 04 Nov 2019 18:43:18 -0300

bruteforce-salted-openssl (1.4.2-1) unstable; urgency=medium

  * New upstream version 1.4.2.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control: bumped Standards-Version to 4.3.0.
  * debian/copyright: updated packaging and upstream copyright years.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 07 Jul 2019 11:07:54 -0300

bruteforce-salted-openssl (1.4.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
      - Updated upstream copyright years.
      - Updated upstream email address.
  * debian/source/include-binaries: added to control debian/tests/test.crypted.
  * debian/tests/*: added to perform a trivial test.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 21 Oct 2018 17:55:58 -0300

bruteforce-salted-openssl (1.4.0-2) unstable; urgency=medium

  [ Joao Eriberto Mota Filho ]
  * Migrated DH level to 11.
  * debian/control: bumped Standards-Version to 4.2.1.
  * debian/copyright:
      - Added rights for Raphaël Hertzog.
      - Updated packaging copyright years.

  [ Raphaël Hertzog ]
  * debian/control:
      - Changed Vcs-* URLs to salsa.debian.org.
      - Updated team maintainer address to Debian Security Tools
        <team+pkg-security@tracker.debian.org>.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Fri, 07 Sep 2018 19:30:56 -0300

bruteforce-salted-openssl (1.4.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: bumped Standards-Version to 4.0.0.
  * debian/copyright: updated the upstream and packaging copyright years.
  * debian/rules: removed no longer needed addon '--with autoreconf'.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 24 Jul 2017 11:58:30 -0300

bruteforce-salted-openssl (1.3.3-1) unstable; urgency=medium

  * New upstream release.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 04 Dec 2016 19:20:32 -0200

bruteforce-salted-openssl (1.3.2-2) unstable; urgency=medium

  * Bump DH level to 10.
  * debian/control: pointed the Vcs-* fields to right place.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Tue, 27 Sep 2016 10:20:07 -0300

bruteforce-salted-openssl (1.3.2-1) unstable; urgency=medium

  * Initial release (Closes: #838848)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 26 Sep 2016 10:10:02 -0300
